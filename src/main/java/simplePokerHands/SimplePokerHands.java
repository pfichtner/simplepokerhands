package simplePokerHands;

import static java.util.Comparator.comparing;
import static java.util.Map.Entry.comparingByKey;
import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.groupingBy;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

public class SimplePokerHands {

	public List<Card> bestCombinationOf(Collection<Card> cards) {
		return cards.stream().collect(groupingBy(Card::getRank)).entrySet()
				.stream()
				.sorted(byValueSize().thenComparing(byRank()).reversed())
				.findFirst().get().getValue();
	}

	private Comparator<Entry<Rank, List<Card>>> byRank() {
		return comparingByKey();
	}

	private Comparator<Entry<Rank, List<Card>>> byValueSize() {
		return comparingByValue(comparing(List::size));
	}

}
