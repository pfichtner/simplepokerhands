package simplePokerHands;

public class Card {

	private final Color color;
	private final Rank rank;

	private Card(Color color, Rank rank) {
		this.color = color;
		this.rank = rank;
	}

	public static Card of(Color color, Rank rank) {
		return new Card(color, rank);
	}

	public static Card parse(String card) {
		return Card.of(Color.of(card.charAt(0)), Rank.rank(card.substring(1)));
	}

	public Rank getRank() {
		return rank;
	}

	public String getRepresentation() {
		return this.color.getRepresentation() + this.rank.getRepresentation();
	}

	@Override
	public int hashCode() {
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (color != other.color)
			return false;
		if (rank != other.rank)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return getRepresentation();
	}

}
