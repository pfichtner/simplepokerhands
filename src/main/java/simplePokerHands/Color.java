package simplePokerHands;

import java.util.stream.Stream;

public enum Color {

	DIAMOND('d'), HEART('h'), SPADE('s'), CLUB('c');

	private final char representation;

	private Color(char representation) {
		this.representation = representation;
	}

	public static Color of(char find) {
		return Stream.of(values()).filter(c -> c.representation == find)
				.findFirst().orElse(null);
	}

	public String getRepresentation() {
		return String.valueOf(this.representation);
	}

}
