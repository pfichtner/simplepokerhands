package simplePokerHands;

import java.util.stream.Stream;

public enum Rank {

	TWO("2"), THREE("3"), FOUR("4"), FIVE("5"), SIX("6"), SEVEN("7"), EIGHT("8"), NINE(
			"9"), TEN("10"), JACK("j"), QUEEN("q"), KING("k"), ACE("a");

	private final String representation;

	private Rank(String representation) {
		this.representation = representation;
	}

	public static Rank rank(int value) {
		return rank(String.valueOf(value));
	}

	public static Rank rank(String value) {
		return Stream
				.of(values())
				.filter(r -> r.representation.equals(value))
				.findFirst()
				.orElseThrow(
						() -> new IllegalArgumentException(value
								+ " not a valid rank"));
	}

	public String getRepresentation() {
		return representation;
	}

}
