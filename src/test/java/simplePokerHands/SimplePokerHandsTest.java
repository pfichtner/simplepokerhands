package simplePokerHands;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static simplePokerHands.Color.CLUB;
import static simplePokerHands.Color.DIAMOND;
import static simplePokerHands.Color.HEART;
import static simplePokerHands.Rank.ACE;
import static simplePokerHands.Rank.JACK;
import static simplePokerHands.Rank.KING;
import static simplePokerHands.Rank.QUEEN;
import static simplePokerHands.SimplePokerHandsTest.CardBuilder.a;
import static simplePokerHands.SimplePokerHandsTest.CardBuilder.aCard;
import static simplePokerHands.hamcrest.RankMatcher.highestCardOfRank;
import static simplePokerHands.hamcrest.RankMatcher.pairOf;
import static simplePokerHands.hamcrest.RankMatcher.threeOfAkind;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

public class SimplePokerHandsTest {

	// sut = system under test
	private final SimplePokerHands sut = new SimplePokerHands();

	static class CardBuilder {

		private Color color;

		private CardBuilder(Color color) {
			this.color = color;
		}

		static CardBuilder aCard() {
			return a(CLUB);
		}

		static CardBuilder a(Color color) {
			return new CardBuilder(color);
		}

		private Card withRank(int value) {
			return withRank(Rank.rank(value));
		}

		Card withRank(Rank rank) {
			return Card.of(color, rank);
		}

		public Color getColor() {
			return color;
		}

	}

	@Test
	public void highestCardOfOneColorSorted() {
		CardBuilder aCard = aCard();
		assertThat(sut.bestCombinationOf(aHandOf(aCard.withRank(2),
				cardOfSameColorLike(aCard).withRank(3))),
				is(highestCardOfRank(3)));
	}

	@Test
	public void highestCardOfOneColorUnsorted() {
		CardBuilder aCard = aCard();
		assertThat(sut.bestCombinationOf(aHandOf(aCard.withRank(3),
				cardOfSameColorLike(aCard).withRank(2))),
				is(highestCardOfRank(3)));
	}

	@Test
	public void highestCardOfTwoColors() {
		CardBuilder aCard = aCard();
		assertThat(
				sut.bestCombinationOf(aHandOf(aCard.withRank(3),
						otherColor(aCard).withRank(2))),
				is(highestCardOfRank(3)));
	}

	@Test
	public void tenGreaterThanNine() {
		assertThat(sut.bestCombinationOf(aHandOf(aCard().withRank(10), aCard()
				.withRank(9))), is(highestCardOfRank(10)));
	}

	@Test
	public void jackIsGreaterThan10() {
		assertThat(sut.bestCombinationOf(aHandOf(aCard().withRank(JACK),
				aCard().withRank(10))), is(highestCardOfRank(JACK)));
	}

	@Test
	public void queenIsGreaterThanJack() {
		assertThat(sut.bestCombinationOf(aHandOf(aCard().withRank(QUEEN),
				aCard().withRank(JACK))), is(highestCardOfRank(QUEEN)));
	}

	@Test
	public void kingIsGreaterThanQueen() {
		assertThat(sut.bestCombinationOf(aHandOf(aCard().withRank(KING),
				aCard().withRank(QUEEN))), is(highestCardOfRank(KING)));
	}

	@Test
	public void aceIsGreaterThanKing() {
		assertThat(sut.bestCombinationOf(aHandOf(aCard().withRank(ACE), aCard()
				.withRank(KING))), is(highestCardOfRank(ACE)));
	}

	@Test
	public void detectsPairOfKings() {
		Card clubKing = a(CLUB).withRank(KING);
		Card heartKing = a(HEART).withRank(KING);
		assertThat(sut.bestCombinationOf(aHandOf(clubKing, heartKing)),
				is(pairOf(KING)));
	}

	@Test
	public void canFindPairOfKingsInHand() {
		Card clubKing = a(CLUB).withRank(KING);
		Card aQueen = cardOfSameColorLike(a(CLUB)).withRank(QUEEN);
		Card heartKing = a(HEART).withRank(KING);
		assertThat(sut.bestCombinationOf(aHandOf(clubKing, aQueen, heartKing)),
				is(pairOf(KING)));
	}

	@Test
	public void canFindThreeOfaKind() {
		Card c10 = a(CLUB).withRank(10);
		Card h10 = a(HEART).withRank(10);
		Card d10 = a(DIAMOND).withRank(10);
		assertThat(sut.bestCombinationOf(aHandOf(c10, h10, d10)),
				is(threeOfAkind(10)));
	}

	@Test
	public void pairOfTwoIsBetterThanOneAce() {
		Card ca = a(CLUB).withRank(ACE);
		Card h2 = a(HEART).withRank(2);
		Card d2 = a(DIAMOND).withRank(2);
		assertThat(sut.bestCombinationOf(aHandOf(ca, h2, d2)), is(pairOf(2)));
	}

	@Test
	public void pairOfThreesIsBetterThanPairOfTwos() {
		Card c2 = a(CLUB).withRank(2);
		Card h2 = a(HEART).withRank(2);
		Card c3 = a(CLUB).withRank(3);
		Card h3 = a(HEART).withRank(3);
		assertThat(sut.bestCombinationOf(aHandOf(c2, h2, c3, h3)),
				is(pairOf(3)));
	}

	private CardBuilder cardOfSameColorLike(CardBuilder cardBuilder) {
		return cardBuilder;
	}

	private Collection<Card> aHandOf(Card... cards) {
		return Arrays.asList(cards);
	}

	private CardBuilder otherColor(CardBuilder otherThan) {
		return CLUB.equals(otherThan.getColor()) ? a(HEART) : a(CLUB);
	}
}
