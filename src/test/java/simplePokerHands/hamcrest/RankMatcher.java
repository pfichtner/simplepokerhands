package simplePokerHands.hamcrest;

import static java.util.stream.Collectors.joining;
import static simplePokerHands.Rank.rank;

import java.util.Collection;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import simplePokerHands.Card;
import simplePokerHands.Rank;

public class RankMatcher extends TypeSafeMatcher<Collection<Card>> {

	private final Rank expected;
	private int count;

	public RankMatcher(Rank expected, int count) {
		this.expected = expected;
		this.count = count;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(count + " card(s) with rank ").appendText(
				expected.toString());
	}

	@Override
	protected void describeMismatchSafely(Collection<Card> cards,
			Description mismatchDescription) {
		mismatchDescription.appendText("got card(s) ").appendText(
				cards.stream().map(Card::getRepresentation)
						.collect(joining(" ")));
	}

	@Override
	protected boolean matchesSafely(Collection<Card> cards) {
		return cards.size() == count
				&& cards.stream().filter(c -> !c.getRank().equals(expected))
						.count() == 0;
	}

	public static RankMatcher highestCardOfRank(int expected) {
		return highestCardOfRank(rank(expected));
	}

	public static RankMatcher highestCardOfRank(Rank expected) {
		return new RankMatcher(expected, 1);
	}

	public static RankMatcher pairOf(int expected) {
		return pairOf(rank(expected));
	}

	public static RankMatcher pairOf(Rank expected) {
		return new RankMatcher(expected, 2);
	}

	public static RankMatcher threeOfAkind(int expected) {
		return threeOfAkind(rank(expected));
	}

	public static RankMatcher threeOfAkind(Rank expected) {
		return new RankMatcher(expected, 3);
	}

}
